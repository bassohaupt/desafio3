package com.calculadoraTemp.calculadoraTemp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalculadoraTempApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalculadoraTempApplication.class, args);
	}

}
