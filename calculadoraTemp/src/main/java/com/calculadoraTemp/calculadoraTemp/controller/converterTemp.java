package com.calculadoraTemp.calculadoraTemp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;



@Controller
public class converterTemp {

	@GetMapping("/")
	public ModelAndView calctemp() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("calctemp");
		return modelAndView;
	}
	@PostMapping("/converter")
	public String converter(@RequestParam("num1") double num1,
							@RequestParam("operator") String operador, 
							Model model) {
		
		double result = 0;
		switch(operador) {
		case "Celsius To Kelvin":
			result = num1 + 273;
			break;
		case "Celsius To Fahrenheit":
			result = (num1 * 1.8) + 32;
			break;
		case "Fahrenheit To Kelvin":
			result = ((num1 - 32) / 1.79999999) + 273.15;
			break;
		case "Fahrenheit To Celsius":
			result = (num1 - 32) / 1.8;
			break;
		case "Kelvin To Fahrenheit":
			result = 1.8 * (num1 - 273) + 32;
			break;
		case "Kelvin To Celsius":
			result = num1 - 273;
			break;
			
		}
		
		model.addAttribute("result", result);
		
		
		return "calctemp";
	}
}
